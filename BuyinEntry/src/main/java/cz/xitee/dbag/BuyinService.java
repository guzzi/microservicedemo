package cz.xitee.dbag;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/")
public class BuyinService {

	@Value("${custom.openshift.hostname}")
	private String hostname;

	@Autowired
	RestTemplate restTemplate;

	@RequestMapping(path = "/auctionRequest", method = RequestMethod.POST)
	public ResponseEntity<Void> sendAuctionRequestion(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("auction", "test");
		restTemplate.postForObject("http://auction." + hostname + ".svc.cluster.local:8090/createAuction", null,
				Void.class, params);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(path = "/getAuction", method = RequestMethod.GET)
	public ResponseEntity<String> getAllAuctions() {
		String result = restTemplate
				.getForObject("http://auction." + hostname + ".svc.cluster.local:8090/getDummyAuction", String.class);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(path = "/callOther", method = RequestMethod.GET)
	public ResponseEntity<String> callOther() {
		String result = restTemplate
				.getForObject("http://auction." + hostname + ".svc.cluster.local:8090/callOtherServices", String.class);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(path = "/dummy", method = RequestMethod.GET)
	public ResponseEntity<String> getDummyPage() {
		return new ResponseEntity<String>("dummy", HttpStatus.OK);
	}
}
